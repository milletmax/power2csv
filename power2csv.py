#!/bin/python3
import sys
import os
import subprocess
import time
import signal
import fcntl
import re
import argparse

def read_all(buff):
    data = ""
    length = 1
    while length > 0:
        r = buff.read1(512).decode()
        length = len(r)
        data = data + r
    return data

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--headers",      "-H", help="Print headers on first line", action='store_true')
    parser.add_argument("--low-priority", "-l", help="Run the process on E-cores", dest="priority", action='store_true')
    parser.add_argument("--sleep",        "-s", help="Time sleep between two measures", type=float, default=0.02)
    args = parser.parse_args()

    if not "SUDO_UID" in os.environ.keys():
        print("power2csv must be invoked as superuser", file=sys.stderr)
        return

    if args.priority:
        # Set PRIO_DARWIN_BG (0x1000) to PRIO_DARWIN_PROCESS (0b100)
        os.setpriority(0b100, 0, 0x1000)

    re_date = re.compile("Sampled system activity \((.*)\) \(")
    re_cpu  = re.compile("CPU Power: (.*) mW")
    re_gpu  = re.compile("GPU Power: (.*) mW")

    with subprocess.Popen(["/usr/bin/powermetrics", "-i0", "-f", "text", "--samplers", "cpu_power"], close_fds=True, stdout=subprocess.PIPE) as pm:
        # Set non blocking io flag on powermetrics' stdout
        fcntl.fcntl(pm.stdout, fcntl.F_SETFL, fcntl.fcntl(pm.stdout, fcntl.F_GETFL) | os.O_NONBLOCK )

        if args.headers:
            print("date;cpu_mW;gpu_mW")

        while True:
            time.sleep(args.sleep)
            os.kill(pm.pid, signal.SIGINFO)
            data = read_all(pm.stdout)
            if len(data) > 0:
                date = re_date.search(data)
                cpu  = re_cpu.search(data)
                gpu  = re_gpu.search(data)
                if date is None or cpu is None or gpu is None:
                    continue
                else:
                    print(date.group(1),cpu.group(1),gpu.group(1),sep=';')

if __name__ == '__main__':
    sys.exit(main())
